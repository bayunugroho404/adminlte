<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

  protected $fillable = [
        'user_id',
        'project_name',
        'brief_description',
        'date_line',
        'status',
        'completion_time'
    ];

        public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

}
