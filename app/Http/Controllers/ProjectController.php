<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\User;


class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $projects = Project::with('user')->get();
       return view('projects.index', [
        'projects' => $projects
    ]);
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();

        return view('projects.create',compact('users'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'user_id' => 'required',
            'project_name' => 'required',
            'brief_description' => 'required',
            'date_line' => 'required'
        ]);


        $project = Project::create([
           'user_id'          => $request->user_id,
           'project_name'    => $request->project_name,
           'brief_description'    => $request->brief_description,
           'date_line'    => $request->date_line,

       ]);     

        return redirect()->route('projects.index')
        ->with('success_message', 'Berhasil menambah Project baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

       $project = Project::find($id);
       $users = User::all();
       if (!$project) return redirect()->route('projects.index')
        ->with('error_message', 'project dengan id '.$id.' tidak ditemukan');
    return view('projects.edit',compact('project','users'));
}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = Project::find($id);
        $project->user_id = $request->user_id;
        $project->project_name = $request->project_name;
        $project->brief_description = $request->brief_description;
        $project->date_line = $request->date_line;
        $project->save();
        return redirect()->route('projects.index')
        ->with('success_message', 'Berhasil mengubah project');
    }


    public function task_done($id)
    {
       $project = Project::find($id);
       $project->status = "done";
        $project->completion_time = date('Y-m-d G:i:s');

       $project->save();
       return redirect()->route('projects.index')
       ->with('success_message', 'berhasil');

   }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $class = Project::findOrFail($id);
        $class->delete();

        if($class){
           return redirect()->route('projects.index')
           ->with('success_message', 'Berhasil menghapus project');
       }else{
        return redirect()->route('projects.index')
        ->with('error_message', 'gagal menghapus project');
    }
}
}
