<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function() {

    /************************* USER *****************************/
    Route::resource('users', \App\Http\Controllers\UserController::class);

    /************************* USER *****************************/
    Route::resource('projects', \App\Http\Controllers\ProjectController::class);

    /************************* PROJECT *****************************/
    Route::resource('users', \App\Http\Controllers\UserController::class);

    Route::prefix('task')->group(function () {
        Route::get('/done/{id}', [App\Http\Controllers\ProjectController::class, 'task_done'])->name('task.done');
    });


    Route::get('/home', function() {
        return view('home');
    })->name('home');

});


