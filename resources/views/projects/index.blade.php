@extends('adminlte::page')

@section('title', 'List Project')

@section('content_header')
<h1 class="m-0 text-dark">List Project</h1>
@stop

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <a href="{{route('projects.create')}}" class="btn btn-primary mb-2">
                    Tambah
                </a>

                <table class="table table-hover table-bordered table-stripped" id="example3">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Anggota</th>
                            <th>Nama Project</th>
                            <th>Brief Description</th>
                            <th>Deadline Project</th>
                            <th>Completion time</th>
                            <th>Status</th>
                            <th>Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($projects as $key => $project)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$project->user->name }}</td>
                            <td>{{$project->project_name}}</td>
                            <td>{{$project->brief_description}}</td>
                            <td>{{$project->date_line }}</td>
                            <td>{{$project->completion_time }}</td>
                            <td>{{$project->status }}</td>

                            <td>
                                <a href="{{route('projects.edit', $project)}}" class="btn btn-secondary btn-xs">
                                    Edit
                                </a>
                                <a href="{{route('projects.destroy', $project)}}" onclick="notificationBeforeDelete(event, this)" class="btn btn-danger btn-xs">
                                    Delete
                                </a>

                                @if($project->status == "progress")

                                <a href="{{route('task.done', $project->id)}}" class="btn btn-primary btn-xs">
                                 Success Progress
                             </a>
                             @endif
                         </td>
                     </tr>
                     @endforeach
                 </tbody>
             </table>

         </div>
     </div>
 </div>
</div>
@stop

@push('js')
<form action="" id="delete-form" method="post">
    @method('delete')
    @csrf
</form>
<script>
    $('#example3').DataTable({
        "responsive": true,
    });

    function notificationBeforeDelete(event, el) {
        event.preventDefault();
        if (confirm('Apakah anda yakin akan menghapus data ? ')) {
            $("#delete-form").attr('action', $(el).attr('href'));
            $("#delete-form").submit();
        }
    }

</script>
@endpush
