@extends('adminlte::page')

@section('title', 'Tambah Project')

@section('content_header')

<h1 class="m-0 text-dark">Tambah Project</h1>
@stop

@section('content')

<form action="{{route('projects.store')}}" method="post">
    @csrf
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">


                  <div class="form-group">
                    <label>Anggota </label>
                    <select name="user_id" class="form-control">
                        <option value="">-- PILIH ANGGOTA --</option>
                        @foreach ($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                    @error('category_id')
                    <div class="invalid-feedback" style="display: block">
                        {{ $message }}
                    </div>
                    @enderror
                </div>


                <div class="form-group">
                    <label for="exampleInputName">Nama Project</label>
                    <input type="text" class="form-control @error('project_name') is-invalid @enderror" id="exampleInputName" placeholder="Nama Project" name="project_name" value="{{old('project_name')}}">
                    @error('project_name') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail">Brief Description</label>
                    <textarea id="exampleInputEmail" class="form-control @error('brief_description') is-invalid @enderror" name="brief_description" value="{{old('brief_description')}}" rows="4" cols="50">

                    </textarea>

                    @error('brief_description') <span class="text-danger">{{$message}}</span> @enderror
                </div>

              
                <div class="form-group">
                    <label for="exampleInputName">Deadline Project</label>
             
                    <input type="text" class="datepicker-here form-control @error('date_line') is-invalid @enderror" data-language='en' data-position='top left' placeholder="Deadline Project" name="date_line" value="{{old('date_line')}}">
                    @error('date_line') <span class="text-danger">{{$message}}</span> @enderror
                </div>

            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{route('users.index')}}" class="btn btn-default">
                    Batal
                </a>
            </div>
        </div>
    </div>
</div>
</form>
@stop
