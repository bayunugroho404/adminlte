@extends('adminlte::page')

@section('title', 'Edit Project')

@section('content_header')
<h1 class="m-0 text-dark">Edit Project</h1>
@stop

@section('content')
<form action="{{route('projects.update', $project)}}" method="post">
    @method('PUT')
    @csrf
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-body">
                    <div class="alert alert-secondary" role="alert">
                        {{ $project->status }}, adalah status saat ini
                  </div>
                  <div class="form-group">
                     <label>ANGGOTA</label>
                     <select name="user_id" class="form-control">
                        <option value="">-- PILIH ANGGOTA --</option>
                        @foreach ($users as $users)
                        @if($project->user_id == $users->id)
                        <option value="{{ $users->id  }}" selected>{{ $users->name }}
                        </option>
                        @else
                        <option value="{{ $users->id  }}">{{ $users->name }}</option>
                        @endif
                        @endforeach
                    </select>
                    @error('user_id')
                    <div class="invalid-feedback" style="display: block">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputName">Nama</label>
                    <input type="text" class="form-control @error('project_name') is-invalid @enderror" id="exampleInputName" placeholder="project name" name="project_name" value="{{$project->project_name ?? old('project_name')}}">
                    @error('project_name') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail">brief description</label>
                    <input type="brief_description" class="form-control @error('brief_description') is-invalid @enderror" id="exampleInputEmail" placeholder=" brief description" name="brief_description" value="{{$project->brief_description ?? old('brief_description')}}">
                    @error('brief_description') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail">dateline</label>
                    <input type="text" class="datepicker-here form-control @error('date_line') is-invalid @enderror" data-language='en' data-position='top left' placeholder="Deadline Project" name="date_line" value="{{$project->date_line ?? old('date_line')}}">

                    @error('date_line') <span class="text-danger">{{$message}}</span> @enderror

                </div>


            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a href="{{route('projects.index')}}" class="btn btn-default">
                    Batal
                </a>
            </div>
        </div>
    </div>
</div>
@stop
